package br.com.thiagogbferreira;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.List;

@Component
@Aspect
public class MicroServiceLogWebfluxApplication {

  private static String getServerRequestHeader(ServerRequest.Headers headers, String key) {
    List<String> values = headers.header(key);
    if (values!=null && values.size()>0) {
      return values.get(0);
    }
    return null;

  }

  @Around("execution(* br.com.thiagogbferreira.*..*(org.springframework.web.reactive.function.server.ServerRequest)) && args(serverRequest)")
  public Object logFlux(ProceedingJoinPoint pjp, ServerRequest serverRequest) throws Throwable {
    return MicroServiceLogApplication.proceed(
        pjp
        , MicroServicesContext.builder()
            .messageId(getServerRequestHeader(serverRequest.headers(), "MessageId"))
            .key(getServerRequestHeader(serverRequest.headers(), "key"))
            .build()
    );
  }

}
